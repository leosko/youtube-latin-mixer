import datetime
import json
from datetime import timedelta
from enum import Enum
from typing import Union


class ShufflerJSONEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, ShuffleType):
            return obj.name.__str__().upper()
        if isinstance(obj, timedelta):
            return obj.total_seconds()
        if isinstance(obj, Genre):
            return obj.name.__str__().upper()
        if isinstance(obj, Source):
            return obj.name.__str__().upper()
        if isinstance(obj, datetime.datetime):
            return obj.isoformat()
        if isinstance(obj, ShufflePlan):
            return obj.to_json()
        return json.JSONEncoder.default(self, obj)


class ShuffleType(Enum):
    NONE = 0
    AVERAGE = 1
    VARIED = 2
    PRIMETIME = 3


class Genre(Enum):
    UNKNOWN = 0
    BACHATA = 1
    SALSA = 2
    KIZOMBA = 3
    CUBATON = 4
    ELECTRONICA = 5
    REGGAETON = 6
    INDIE = 7
    DANCE = 8

    def short_str(self):
        return self.name[0]


class Source(Enum):
    UNKNOWN = 0
    YOUTUBE = 1
    SPOTIFY = 2
    CUSTOM_SPOTIFY = 3
    CUSTOM_YOUTUBE = 4


class BlockShufflePlan:
    def __init__(self, genre: Genre = None, count: int = 0):
        self.genre: Union[Genre, None] = genre
        self.count: int = count

    def to_json(self):
        return {
            'genre': self.genre,
            'count': self.count,
        }


class TypeShufflePlan:
    def __init__(self, type: ShuffleType = None, time_start: timedelta = None):
        self.type: Union[ShuffleType, None] = type
        self.time_start: Union[timedelta, None] = time_start

    def to_json(self):
        return {
            'type': self.type,
            'time_start': str(self.time_start),
        }


def parse_duration(hhmmss: str):
    t = datetime.datetime.strptime(hhmmss, '%H:%M:%S')
    return timedelta(hours=t.hour, minutes=t.minute, seconds=t.second)


class ShufflePlan:

    def __init__(self, json_obj: dict = None):
        self.blocks = [BlockShufflePlan(genre=Genre[genre['genre'].upper()], count=genre['count']) for genre in json_obj['blocks']]
        self.types = [TypeShufflePlan(type=ShuffleType[type['type'].upper()], time_start=parse_duration(type['time_start'])) for type in json_obj['types']]
        self.max_duration = parse_duration(json_obj['max_duration'])

    def to_json(self):
        return {
            'genres': [genre.to_json() for genre in self.blocks],
            'types': [type.to_json() for type in self.types],
            'max_duration': self.max_duration,
        }


