import json
from typing import Union, Dict

from Levenshtein import distance

from database import Database
from shuffle_plan import Source, Genre
from spotify import Spotify
from yt_music import YTMusic


def guess_genre(yt_track):
    if yt_track['title'].lower().find('bachata') != -1:
        return Genre.BACHATA
    if yt_track['title'].lower().find('salsa') != -1:
        return Genre.SALSA
    if yt_track['title'].lower().find('kizomba') != -1:
        return Genre.KIZOMBA
    return Genre.UNKNOWN


def parse_view_count(view_count: str) -> int:
    if view_count.endswith('K') or view_count.endswith('k'):
        return int(float(view_count[:-1]) * 1000)
    elif view_count.endswith('M') or view_count.endswith('m'):
        return int(float(view_count[:-1]) * 1000000)
    else:
        return int(view_count)


def get_track_from_yt_track(yt_track, yt_meta):
    res = {
        'video_id': yt_track['videoId'],
        'json_from_youtube': yt_track,
        'genre': guess_genre(yt_track),
        'title': yt_track['title'],
        'artist': yt_track['artists'][0]['name'],
        'priority': None,
        'bpm': None,
        'mood': None,
        'spotify_id': None,
        'data_from_spotify': None,
        'danceability': None,
        'energy': None,
        'key': None,
        'loudness': None,
        'mode': None,
        'instrumentalness': None,
        'acousticness': None,
        'liveness': None,
        'valence': None,
        'duration_seconds': None,
        'speechiness': None,
        'verified': 0,
        'unavailable_since': None,
        'source': Source.YOUTUBE,
        'published_at': 0,
        'view_count': 0,
        'popularity': 0,
    }
    if 'microformat' in yt_meta and 'microformatDataRenderer' in yt_meta['microformat']:
        res['published_at'] = yt_meta['microformat']['microformatDataRenderer']['publishDate']
        res['view_count'] = parse_view_count(yt_meta['microformat']['microformatDataRenderer']['viewCount'])
    return res


def get_track(yt_track, yt_meta, spotify_track, spotify_audio_features, genre=None, source=Source.UNKNOWN):
    return {
        'video_id': yt_track['videoId'],
        'json_from_youtube': yt_track,
        'genre': guess_genre(yt_track) if genre is None else Genre[genre.upper()],
        'title': yt_track['title'],
        'artist': yt_track['artists'][0]['name'] if 'artists' in yt_track and len(yt_track['artists']) > 0 else None,
        'priority': None,
        'bpm': spotify_audio_features['tempo'],
        'mood': None,
        'spotify_id': spotify_track['id'],
        'data_from_spotify': spotify_audio_features,
        'danceability': spotify_audio_features['danceability'],
        'energy': spotify_audio_features['energy'],
        'key': spotify_audio_features['key'],
        'loudness': spotify_audio_features['loudness'],
        'mode': spotify_audio_features['mode'],
        'instrumentalness': spotify_audio_features['instrumentalness'],
        'acousticness': spotify_audio_features['acousticness'],
        'liveness': spotify_audio_features['liveness'],
        'valence': spotify_audio_features['valence'],
        'duration_seconds': yt_track['duration_seconds'],
        'speechiness': spotify_audio_features['speechiness'],
        'verified': 0,
        'unavailable_since': None,
        'source': source,
        'published_at': yt_meta['microformat']['microformatDataRenderer']['publishDate'][:10],
        'view_count': parse_view_count(yt_meta['microformat']['microformatDataRenderer']['viewCount']),
        'popularity': spotify_track['popularity'],
    }


class Importer:
    def __init__(self, database: Database, spotify: Spotify, yt_music: YTMusic):
        self.database = database
        self.spotify = spotify
        self.yt_music = yt_music

    def load_from_api_by_url(self, url, source=Source.UNKNOWN):
        if url.startswith("https://www.youtube.com/watch?v=") or url.startswith("https://music.youtube.com/watch?v="):
            yt_id = url.split("=")[1].split("&")[0]
            track = self.database.get_track_by_yt_video_id(yt_id)
            if track:
                raise Exception(f"Track already exists in DB: {track}")

            search = self.yt_music.get_api().search(yt_id, filter='songs')
            if len(search) == 0:
                search = self.yt_music.get_api().search(yt_id, filter='videos')
            if len(search) == 0:
                raise Exception(f"Can't find {url} on YouTube")
            yt_track = search[0]
            if not yt_track:
                raise Exception(f"Can't find {url} on YouTube")
            spotify_track = self.spotify.get_track_by_yt_track(yt_track)
            if not spotify_track:
                raise Exception(f"Can't find {url} on Spotify, yt_track={yt_track}")
            spotify_id = spotify_track['id']

            if source == Source.UNKNOWN:
                source = Source.CUSTOM_YOUTUBE
        elif url.startswith("https://open.spotify.com/track/"):
            spotify_id = url.split("/")[-1].split("?")[0]
            track = self.database.get_track_by_spotify_id(spotify_id)
            if track:
                raise Exception(f"Track already exists in DB: {track}")

            spotify_track = self.spotify.get_api().track(spotify_id)
            if not spotify_track:
                raise Exception(f"Can't find {url} on Spotify")
            yt_track = self.yt_music.get_track_by_spotify_track(spotify_track)
            if not yt_track:
                raise Exception(f"Can't find {url} on YouTube")
            yt_id = yt_track['videoId']

            if source == Source.UNKNOWN:
                source = Source.CUSTOM_SPOTIFY
        else:
            raise Exception(f"Unknown url {url}")

        return yt_id, yt_track, spotify_id, spotify_track, source

    def import_track(self, url=None, genre=None, source=Source.UNKNOWN,
                     yt_id=None, spotify_id=None, yt_track=None, spotify_track=None):
        if url:
            yt_id, yt_track, spotify_id, spotify_track, source = self.load_from_api_by_url(url, source)
        elif yt_id:
            yt_id, yt_track, spotify_id, spotify_track, source = self.load_from_api_by_url(f'https://www.youtube.com/watch?v={yt_id}', source)
        elif spotify_id:
            yt_id, yt_track, spotify_id, spotify_track, source = self.load_from_api_by_url(f'https://open.spotify.com/track/{spotify_id}', source)
        elif yt_track:
            yt_id, yt_track, spotify_id, spotify_track, source = self.load_from_api_by_url(f'https://www.youtube.com/watch?v={yt_track["videoId"]}', source)
        elif spotify_track:
            yt_id, yt_track, spotify_id, spotify_track, source = self.load_from_api_by_url(f'https://open.spotify.com/track/{spotify_track["id"]}', source)

        yt_meta = self.yt_music.get_api().get_song(yt_id)
        if not yt_meta:
            raise Exception(f"Can't find {url} meta on YouTube")
        spotify_audio_features = self.spotify.get_audio_features(spotify_id)
        if not spotify_audio_features:
            raise Exception(f"Can't find {url} audio features on Spotify")

        # compare duration
        if abs(yt_track['duration_seconds'] - spotify_audio_features['duration_ms'] / 1000) > 2:
            print(f"WARNING: Duration mismatch for {url}: yt={yt_track['duration_seconds']} spotify={spotify_audio_features['duration_ms'] / 1000}")

        track = get_track(yt_track, yt_meta, spotify_track, spotify_audio_features, genre, source)
        self.database.insert_track(track)

    def preprocess_tags_for_all_tracks(self):
        for track in self.database.get_tracks():
            if self.preprocess_tags(track):
                self.database.update_track(track)

    def sync_tracks_to_db(self, combined_playlist):
        video_ids_from_db = {track['video_id']: track for track in self.database.get_tracks()}
        video_ids_from_youtube = {track['videoId']: track for track in combined_playlist['tracks']}

        for yt_track in combined_playlist['tracks']:
            self.sync_track_to_db(video_ids_from_db, yt_track)

        for track in self.database.get_tracks():
            if (track["video_id"] not in video_ids_from_youtube and
                    track['priority'] != -1 and
                    track['source'] == Source.YOUTUBE):
                print(f"Track {track} exists in DB, but not in playlist, setting it to low priority")
                track['priority'] = -1
                self.database.update_track(track)

    def sync_track_to_db(self, video_ids_from_db, yt_track):
        if yt_track['videoId'] not in video_ids_from_db:
            another_track = self.database.get_closest_track_by_artist_title(yt_track['artists'][0]['name'] if 'artists' in yt_track and len(yt_track['artists']) > 0 else None,
                                                                            yt_track['title'],
                                                                            yt_track['album']['name'] if ('album' in yt_track and yt_track['album']) else None)
            if another_track:
                if not another_track['verified']:
                    print(
                        f"WARNING: There is a track with very close artist and title but different videoId: new videoId={yt_track['videoId']} old videoId={another_track['video_id']}")
                    print(f"New track: {yt_track}")
                    print(f"Old track: {another_track}")
                return None
            return self.import_track(yt_track=yt_track, source=Source.YOUTUBE)

    def preprocess_tags(self, track_from_db):
        if track_from_db['bpm'] is not None and track_from_db['danceability'] is not None:
            return False
        print(f"Preprocessing tags for {track_from_db['video_id']}...")

        if track_from_db['data_from_spotify'] is None:
            track_from_db['data_from_spotify'] = self.spotify.get_audio_features(track_from_db)
            track_from_db['bpm'] = track_from_db['data_from_spotify']['tempo']
            track_from_db['spotify_id'] = track_from_db['data_from_spotify']['id']
            track_from_db['danceability'] = track_from_db['data_from_spotify']['danceability']
            track_from_db['energy'] = track_from_db['data_from_spotify']['energy']
            track_from_db['key'] = track_from_db['data_from_spotify']['key']
            track_from_db['loudness'] = track_from_db['data_from_spotify']['loudness']
            track_from_db['mode'] = track_from_db['data_from_spotify']['mode']
            track_from_db['speechiness'] = track_from_db['data_from_spotify']['speechiness']
            track_from_db['acousticness'] = track_from_db['data_from_spotify']['acousticness']
            track_from_db['instrumentalness'] = track_from_db['data_from_spotify']['instrumentalness']
            track_from_db['liveness'] = track_from_db['data_from_spotify']['liveness']
            track_from_db['valence'] = track_from_db['data_from_spotify']['valence']
            track_from_db['popularity'] = self.spotify.get_popularity(track_from_db)

        track_from_db['duration_seconds'] = track_from_db['json_from_youtube']['duration_seconds']

        print(f"Preprocessing tags for {track_from_db['video_id']} finished: {track_from_db}")
        return True

    def update_published_at(self):
        for track in self.database.get_tracks():
            if track['source'] == Source.YOUTUBE or track['source'] == Source.CUSTOM_SPOTIFY:
                yt_data = self.yt_music.get_api().get_song(track['video_id'])
                track['published_at'] = yt_data['microformat']['microformatDataRenderer']['publishDate']
                self.database.update_track(track)

    def update_spotify_popularity(self):
        tracks = self.database.get_tracks()
        new_popularities = self.spotify.get_popularities(tracks)
        print(f"New popularities: {new_popularities}")
        for track, new_popularity in zip(tracks, new_popularities):
            if new_popularity != track['popularity']:
                print(f"Updating popularity for {track['video_id']} from {track['popularity']} to {new_popularity}")
                track['popularity'] = self.spotify.get_popularity(track)
                self.database.update_track(track)

    def update_view_counts(self):
        for track in self.database.get_tracks():
            if track['unavailable_since'] is not None:
                continue
            if track['source'] == Source.YOUTUBE or track['source'] == Source.CUSTOM_SPOTIFY:
                yt_data = self.yt_music.get_api().get_song(track['video_id'])
                if not yt_data or 'microformat' not in yt_data or 'microformatDataRenderer' not in yt_data['microformat']:
                    print(f"Can't find view count for {track['video_id']}")
                    continue
                new_view_count = parse_view_count(yt_data['microformat']['microformatDataRenderer']['viewCount'])
                if new_view_count != track['view_count'] and new_view_count / track['view_count'] > 1.2:
                    print(f"Updating view count for {track['video_id']} from {track['view_count']} to {new_view_count}")
                    track['view_count'] = new_view_count
                    self.database.update_track(track)

    def update_spotify_id(self, video_id, new_spotify_id):
        track = self.database.get_track_by_yt_video_id(video_id)
        if not track:
            raise Exception(f"Can't find track with video_id={video_id}")

        track = get_track(track['json_from_youtube'], self.yt_music.get_api().get_song(video_id),
                          self.spotify.get_api().track(new_spotify_id),
                          self.spotify.get_audio_features(new_spotify_id),
                          track['genre'].name, track['source'])
        self.database.update_track(track)

    def update_youtube_id(self, spotify_id, new_youtube_id):
        track = self.database.get_track_by_spotify_id(spotify_id)
        old_youtube_id = track['video_id']
        if not track:
            raise Exception(f"Can't find track with spotify_id={spotify_id}")

        track = get_track(self.yt_music.get_api().search(new_youtube_id)[0],
                          self.yt_music.get_api().get_song(new_youtube_id),
                          self.spotify.get_api().track(spotify_id),
                          track['data_from_spotify'],
                          track['genre'].name, track['source'])
        self.database.delete_track(old_youtube_id)
        self.database.insert_track(track)
