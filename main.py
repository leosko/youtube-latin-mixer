import argparse
import json
import os
import time
import traceback

from database import Database

from downloader import Downloader
from importer import Importer
from shuffle_plan import ShufflePlan, ShufflerJSONEncoder
from shuffler import Shuffler, dump_tracks
from spotify import Spotify
from yt_music import YTMusic

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Shuffle some music.')
    parser.add_argument_group('Shuffling')
    parser.add_argument('--youtube-playlists', '-yt', default=['PLtJG2ryNCCmJbnOHsxsrbXqfV3llEr34t'], nargs='+', help='YouTube playlists to sync')
    parser.add_argument('--skip-publish', '--dry-run', default=False, action=argparse.BooleanOptionalAction, help='Shuffle only, do not create new playlist')
    parser.add_argument('--shuffle-plan-path', default='shuffle-plan.json', help='Shuffle plan JSON file')
    parser.add_argument('--seed', default=None, help='Seed for shuffling')
    parser.add_argument('--available-sources', default=['youtube', 'spotify', 'custom_spotify', 'custom_youtube'], nargs='+', help='Available sources')
    parser.add_argument_group('Syncing')
    parser.add_argument('--skip-load-from-youtube', default=False, action=argparse.BooleanOptionalAction, help='Sync tracks from YouTube')
    parser.add_argument('--skip-load-spotify-tags', default=False, action=argparse.BooleanOptionalAction, help='Sync tags from Spotify')
    parser.add_argument_group('Filepaths')
    parser.add_argument('--youtube-api-path', default='oauth.json', help='YouTube API keys file')
    parser.add_argument('--spotify-api-path', default='spotify_app.json', help='Spotify API keys file')
    parser.add_argument('--database-path', default='tracks.db', help='Database file')
    parser.add_argument_group('Downloading')
    parser.add_argument('--library-path', default='latin_mixer' + os.sep + 'library', help='Path to download library to')
    parser.add_argument('--skip-download', default=False, action=argparse.BooleanOptionalAction, help='Skip downloading tracks')
    parser.add_argument('--offline-generation-path', default='latin_mixer' + os.sep + 'generated', help='Offline generation target folder')
    parser.add_argument('--concurrency', default=8, type=int, help='Number of threads to use for downloading')
    parser.add_argument_group('Custom tracks')
    parser.add_argument('--add-track', '-add', '-a', default=None, nargs='*', help='Add custom track to database by URL or other identifier')
    parser.add_argument_group('Migrations')
    parser.add_argument('--update-spotify-id', default=None, help='Update Spotify ID for track and reload tags')
    parser.add_argument('--update-youtube-id', default=None, help='Update YouTube ID for track and reload tags')
    parser.add_argument('--update-view-counts', default=False, action=argparse.BooleanOptionalAction, help='Update view counts from YouTube')
    parser.add_argument('--update-published-at', default=False, action=argparse.BooleanOptionalAction, help='Update published at from YouTube')
    parser.add_argument('--update-spotify-popularity', default=False, action=argparse.BooleanOptionalAction, help='Update Spotify popularity')
    parser.add_argument('--check-duration', default=False, action=argparse.BooleanOptionalAction, help='Check duration of all tracks')
    parser.add_argument_group('Misc')
    parser.add_argument('--list', default=False, action=argparse.BooleanOptionalAction, help='List tracks links from database')

    args = parser.parse_args()

    yt_music = YTMusic(args.youtube_api_path)
    database = Database(args.database_path)
    spotify = Spotify(args.spotify_api_path)
    importer = Importer(database, spotify, yt_music)
    shuffler = Shuffler(database, importer, yt_master_playlist_ids=args.youtube_playlists,
                        available_sources=args.available_sources, seed=args.seed)
    downloader = Downloader(database, args.library_path, args.concurrency)
    if args.list:
        for track in database.get_tracks():
            print(f"[{track['verified']}] {track['_spotify_url']} {track['_youtube_url']} {track['genre'].name} {track['artist']} - {track['title']} from {track['source'].name}")
        exit(0)
    if args.check_duration:
        count = 0
        for track in database.get_tracks():
            if abs(track['data_from_spotify']['duration_ms'] / 1000 - track['duration_seconds']) > 2 and not track['verified']:
                count += 1
                print(f"{track['video_id']} duration mismatch, from YT: {track['duration_seconds']} from spotify: {track['data_from_spotify']['duration_ms'] / 1000}: {track['artist']} - {track['title']}, {track['genre'].name}, {track['source'].name}, {track['_spotify_url']}, {track['_youtube_url']}")
        print(f"Total {count} ({round(count / len(database.get_tracks()) * 100)}%) tracks with unverified duration mismatch")
        exit(0)
    if args.update_view_counts:
        importer.update_view_counts()
        exit(0)
    if args.update_published_at:
        importer.update_published_at()
        exit(0)
    if args.update_spotify_popularity:
        importer.update_spotify_popularity()
        exit(0)
    if args.update_spotify_id:
        importer.update_spotify_id(args.update_spotify_id.split('~')[0], args.update_spotify_id.split('~')[1])
        exit(0)
    if args.update_youtube_id:
        importer.update_youtube_id(args.update_youtube_id.split('~')[0], args.update_youtube_id.split('~')[1])
        exit(0)
    if args.add_track:
        for track in args.add_track:
            split = track.split('~')
            try:
                if len(split) == 1:
                    importer.import_track(split[0])
                else:
                    genre = split[0]
                    url = split[1]
                    importer.import_track(url, genre)
            except Exception as e:
                print(f"Failed to import {track}: {e}")
                print(traceback.format_exc())
        database.print_stats()
        exit(0)
    if not args.skip_load_from_youtube:
        importer.sync_tracks_to_db(shuffler.get_master_playlist())
    if not args.skip_load_spotify_tags:
        importer.preprocess_tags_for_all_tracks()
    if not args.skip_download:
        downloader.download_tracks(database.get_tracks())

    with open(args.shuffle_plan_path, 'r') as f:
        shuffle_plan = ShufflePlan(json.load(f))

    shuffled_playlist = shuffler.generate_shuffle(shuffle_plan)
    print('\n'.join(list(
        map(lambda x: f"{x['weight']} {x['time_spent']} {x['video_id']} {x['genre'].name} {x['artist']} - {x['title']}",
            shuffled_playlist))))

    # Check for duplicates
    video_ids = set()
    for track in shuffled_playlist:
        if track['video_id'] in video_ids:
            print(f"Duplicate video_id: {track['video_id']}")
            exit(1)
        video_ids.add(track['video_id'])

    if args.offline_generation_path:
        dump_tracks(shuffled_playlist, args.offline_generation_path, downloader)
        with open(args.offline_generation_path + os.sep + 'trace_generated.txt', 'w', encoding='utf-8') as f:
            f.write('\n'.join(list(map(lambda x: json.dumps(x, cls=ShufflerJSONEncoder), shuffler.selection_trace))))
            f.flush()
            f.close()

    if not args.skip_publish:
        new_yt_playlist_id = yt_music.create_playlist(f"Latin Shuffle {time.strftime('%d-%m-%Y')}",
                                                      list(map(lambda x: x['video_id'], shuffled_playlist)), shuffle_plan,
                                                      {'seed': shuffler.seed})
        print(f"New playlist URL: https://www.youtube.com/playlist?list={new_yt_playlist_id}")
