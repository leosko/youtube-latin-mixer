import copy
import itertools
import os
import random
import shutil
from datetime import datetime, timedelta
from typing import Dict, List

from database import Database

from downloader import Downloader
from importer import Importer
from shuffle_plan import ShufflePlan, ShuffleType, Source, Genre


def progress_hook(d):
    if d['status'] == 'finished':
        print('Done downloading from YouTube, now converting to music file...')


def get_shuffle_plan_type(plan: ShufflePlan, time_spent):
    for i in range(len(plan.types) - 1):
        if plan.types[i].time_start <= time_spent < plan.types[i + 1].time_start:
            return plan.types[i].type
    return plan.types[-1].type


def dump_tracks(shuffled_playlist, offline_generation_path, downloader: Downloader):
    if os.path.exists(offline_generation_path + os.sep + 'generated.txt'):
        shutil.rmtree(offline_generation_path)
    if not os.path.exists(offline_generation_path):
        os.makedirs(offline_generation_path)
    with open(offline_generation_path + os.sep + 'generated.txt', 'w', encoding='utf-8') as f:
        f.write('\n'.join(list(
            map(lambda
                    x: f"{x['number']} {x['weight']} {x['time_spent']} {x['video_id']} {x['genre'].name} {x['artist']} - {x['title']}",
                shuffled_playlist))))
        print(f"Playlist written to {offline_generation_path + os.sep + 'generated.txt'}")
    for track in shuffled_playlist:
        src = downloader.get_path(track)
        if not os.path.exists(src):
            print(f"WARNING: {src} does not exist")
            continue
        shutil.copy2(src, f"{offline_generation_path}{os.sep}{track['number']:03} - {os.path.basename(src)}")


class Shuffler:
    def __init__(self, database: Database, importer: Importer, yt_master_playlist_ids: List,
                 available_sources: List, seed=None):
        self.database = database
        self.importer = importer
        self.yt_master_playlist_ids = yt_master_playlist_ids
        self.available_sources = list(map(lambda x: Source[x.upper()], available_sources))
        self.master_playlist_combined = None

        self.selection_trace = []
        self.selection_trace_idx = itertools.count()

        if seed is not None:
            self.seed = seed
        else:
            self.seed = datetime.now().microsecond
        random.seed(self.seed)

    def get_master_playlist(self) -> Dict:
        if self.master_playlist_combined is None:
            self.master_playlist_combined = self.load_master_playlist()
        return self.master_playlist_combined

    def load_master_playlist(self):
        master_playlist_combined = {'tracks': []}
        for yt_playlist_id in self.yt_master_playlist_ids:
            master_playlist = self.importer.yt_music.get_api().get_playlist(yt_playlist_id, limit=None)
            master_playlist['tracks'] = list(filter(lambda x: x['isAvailable'], master_playlist['tracks']))
            master_playlist_combined['tracks'].extend(master_playlist['tracks'])
        print(f"Loaded {len(master_playlist_combined['tracks'])} tracks from YT")
        return master_playlist_combined

    def get_shuffle_average_values(self, shuffle, tail_size=5):
        if shuffle is None or len(shuffle) == 0:
            return {
                'bpm': 130,
                'danceability': 0.5,
                'energy': 0.5,
                'loudness': 0.5,
                'instrumentalness': 0.5,
                'acousticness': 0.5,
                'liveness': 0.5,
                'valence': 0.5,
                'speechiness': 0.5,
            }
        bpm = 0
        danceability = 0
        energy = 0
        loudness = 0
        instrumentalness = 0
        acousticness = 0
        liveness = 0
        valence = 0
        speechiness = 0
        for track in shuffle[-tail_size:]:
            bpm += track['bpm']
            danceability += track['danceability']
            energy += track['energy']
            loudness += track['loudness']
            instrumentalness += track['instrumentalness']
            acousticness += track['acousticness']
            liveness += track['liveness']
            valence += track['valence']
            speechiness += track['speechiness']
        return {
            'bpm': bpm / len(shuffle),
            'danceability': danceability / len(shuffle),
            'energy': energy / len(shuffle),
            'loudness': loudness / len(shuffle),
            'instrumentalness': instrumentalness / len(shuffle),
            'acousticness': acousticness / len(shuffle),
            'liveness': liveness / len(shuffle),
            'valence': valence / len(shuffle),
            'speechiness': speechiness / len(shuffle),
        }

    def get_track_weight(self, track, current_shuffle_average_values, shuffle_type=ShuffleType.NONE):
        res = 1.0
        if track['priority'] is not None:
            res += track['priority'] * 5000
        if track['verified']:
            res += 10000
        match shuffle_type:
            case ShuffleType.NONE:
                pass
            case ShuffleType.AVERAGE:
                if track['bpm'] is not None:  # TODO
                    res += (130 + track['bpm'] - self.database.get_average_values()['bpm']) * 100
                res += (1 + track['danceability'] - self.database.get_average_values()['danceability']) * 2000
                res += (1 + track['energy'] - self.database.get_average_values()['energy']) * 2000
                res += (1 + track['loudness'] - self.database.get_average_values()['loudness']) * 2000
                res += (1 + track['instrumentalness'] - self.database.get_average_values()['instrumentalness']) * 2000
                res += (1 + track['acousticness'] - self.database.get_average_values()['acousticness']) * 2000
                res += (1 + track['liveness'] - self.database.get_average_values()['liveness']) * 2000
                res += (1 + track['valence'] - self.database.get_average_values()['valence']) * 2000
                res += (1 + track['speechiness'] - self.database.get_average_values()['speechiness']) * 2000
            case ShuffleType.VARIED:
                res += abs(track['bpm'] - current_shuffle_average_values['bpm']) * 200
                res += abs(track['danceability'] - current_shuffle_average_values['danceability']) * 200
                res += abs(track['energy'] - current_shuffle_average_values['energy']) * 200
                res += abs(track['loudness'] - current_shuffle_average_values['loudness']) * 200
                res += abs(track['instrumentalness'] - current_shuffle_average_values['instrumentalness']) * 200
                res += abs(track['acousticness'] - current_shuffle_average_values['acousticness']) * 200
                res += abs(track['liveness'] - current_shuffle_average_values['liveness']) * 200
                res += abs(track['valence'] - current_shuffle_average_values['valence']) * 200
                res += abs(track['speechiness'] - current_shuffle_average_values['speechiness']) * 200
            case ShuffleType.PRIMETIME:
                res += abs(track['bpm'] - current_shuffle_average_values['bpm']) * 200  # vary the BPM
                res += track['danceability'] * 3000
                res += track['energy'] * 2000
                # res += abs(track['loudness'] - current_shuffle_average_values['loudness']) * 20
                # res += abs(track['instrumentalness'] - current_shuffle_average_values['instrumentalness']) * 20
                res += abs(track['acousticness'] - current_shuffle_average_values['acousticness']) * 2000
                res += abs(track['liveness'] - current_shuffle_average_values['liveness']) * 2000
                # res += abs(track['valence'] - current_shuffle_average_values['valence']) * 20
                res += track['speechiness'] * 2000
                res += track['index'] * 500  # latest added tracks are better
                if track['_age'].days < 5 * 365:  # prefer newer tracks in general
                    res += (5 * 365 - track['_age'].days) / 365 * 3000
                if track['popularity'] is not None:  # prefer more popular tracks (based on Spotify)
                    res += track['popularity'] * 150
        return round(res)

    def weighted_random_choice(self, tracks):
        total_weight = sum(track['weight'] for track in tracks)
        rand_value = random.uniform(0, total_weight)
        initial_rand_value = rand_value

        index = 0
        for track in tracks:
            rand_value -= track['weight']
            if rand_value <= 0:
                self.log(
                    {'initial_rand_value': initial_rand_value, 'rand_value': rand_value, 'total_weight': total_weight,
                     'index': index, 'weighted_random_chosen_track': track, })
                return track
            index += 1

        self.log({'fallback': True, 'initial_rand_value': initial_rand_value, 'rand_value': rand_value,
                  'total_weight': total_weight, 'index': index, 'weighted_random_chosen_track': tracks[-1]})
        # Fallback: return the last track if for some reason the random selection fails
        return tracks[-1]

    def log(self, msg):
        self.selection_trace.append({'time': next(self.selection_trace_idx), 'log': copy.deepcopy(msg)})

    def recalculate_sort_weights(self, tracks_from_db_single_genre, current_shuffle, shuffle_plan_type: ShuffleType):
        if tracks_from_db_single_genre is None or len(tracks_from_db_single_genre) == 0:
            return

        current_shuffle_average_values = self.get_shuffle_average_values(current_shuffle)
        for track in tracks_from_db_single_genre:
            track['weight'] = self.get_track_weight(track, current_shuffle_average_values, shuffle_plan_type)

        # normalize weights
        min_weight = min(track['weight'] for track in tracks_from_db_single_genre)
        for track in tracks_from_db_single_genre:
            track['weight'] -= min_weight

        tracks_from_db_single_genre.sort(key=lambda t: t['weight'], reverse=True)

    def get_tracks_split_by_genre_and_sorted(self):
        res = dict()
        available_tracks = list(filter(lambda x: x['unavailable_since'] is None
                                                 and x['source'] in self.available_sources
                                                 and (x['priority'] is None or x['priority'] > 0),
                                       self.database.get_tracks()))
        for track in available_tracks:
            if track['genre'] not in res:
                res[track['genre']] = []
            res[track['genre']].append(track)

        return res

    def generate_shuffle(self, plan: ShufflePlan) -> List[Dict]:
        by_genre = self.get_tracks_split_by_genre_and_sorted()
        requested_genres = list(map(lambda x: x.genre, plan.blocks))
        # get count of unique genres
        tracks_left = sum(len(by_genre.get(genre, [])) for genre in set(requested_genres))

        res = []
        time_spent = timedelta(seconds=0)
        current_shuffle_type = ShuffleType.NONE
        current_sequence_idx = 0
        current_shuffle_type_idx = 0
        self.log({'tracks_left': tracks_left, 'genres': requested_genres, 'current_shuffle_type': current_shuffle_type})
        while tracks_left > 0 and time_spent < plan.max_duration:
            genre = plan.blocks[current_sequence_idx].genre
            shuffle_plan_type = get_shuffle_plan_type(plan, time_spent)
            self.log(
                {'shuffle_plan_type': shuffle_plan_type, 'genre': genre, 'current_sequence_idx': current_sequence_idx})
            tracks = by_genre.get(genre, [])

            for i in range(plan.blocks[current_sequence_idx].count):
                self.recalculate_sort_weights(tracks, list(filter(lambda x: x['genre'] == genre, res)),
                                              shuffle_plan_type)
                self.log({'weights': [f"{track['weight']}" for track in tracks]})
                if not tracks:
                    print(f"WARNING: No tracks for genre {genre}")
                    self.log({'current_sequence_idx': current_sequence_idx, 'skipped': "no tracks for genre",
                              'genre': genre})
                    break

                if not tracks:
                    print(f"WARNING: Not enough tracks for genre {genre}")
                    self.log({'skipped': "not enough tracks for genre", 'genre': genre})
                    break
                track_from_db = self.weighted_random_choice(tracks)
                time_spent += timedelta(seconds=track_from_db['duration_seconds'])
                shuffled_track = copy.deepcopy(track_from_db)
                shuffled_track['time_spent'] = time_spent
                shuffled_track['number'] = len(res) + 1
                res.append(shuffled_track)
                tracks.remove(track_from_db)
                tracks_left -= 1
                self.log({'shuffled_track': shuffled_track, 'time_spent': time_spent, 'tracks_left': tracks_left})
                print(
                    f"Added track {res[-1]['video_id']} {res[-1]['artist']} - {res[-1]['title']} to playlist, {tracks_left} {genre.name} tracks left, {time_spent} spent, {shuffle_plan_type.name}")
            current_sequence_idx = (current_sequence_idx + 1) % len(plan.blocks)
        return res
