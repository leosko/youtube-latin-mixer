import json
from typing import Union, Dict

from ytmusicapi import YTMusic as YTMusicAPI

from shuffle_plan import ShufflerJSONEncoder


class YTMusic:
    def __init__(self, api_path="oauth.json"):
        self.yt_music = None
        self.api_path = api_path

    def get_api(self) -> YTMusicAPI:
        if self.yt_music is None:
            self.yt_music = YTMusicAPI(self.api_path)
        return self.yt_music

    def create_playlist(self, title, video_ids, shuffle_plan, info=None) -> Union[str, Dict]:
        if info is None:
            info = {}
        info['shuffle_plan'] = shuffle_plan
        # description = base64.b64encode(zlib.compress(pickle.dumps(info), level=9)).decode('utf-8')
        description = json.dumps(info, cls=ShufflerJSONEncoder)
        return self.get_api().create_playlist(title=title,
                                              description=description,
                                              privacy_status="UNLISTED",
                                              video_ids=video_ids)

    def get_track_by_spotify_track(self, spotify_track) -> dict:
        return self.get_api().search(query=f'{spotify_track["artists"][0]["name"]} {spotify_track["name"]}', filter='songs')[0]