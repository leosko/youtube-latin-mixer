import json
import os

import spotipy
from spotipy import SpotifyClientCredentials


class Spotify:
    def __init__(self, api_path="spotify_app.json"):
        self.sp = None
        self.api_path = api_path

    def get_api(self) -> spotipy.Spotify:
        if not os.path.exists(self.api_path):
            raise Exception("spotify_app.json not found")
        if not self.sp:
            with open(self.api_path, "r") as f:
                spotify_app = json.load(f)
                sp = spotipy.Spotify(auth_manager=SpotifyClientCredentials(client_id=spotify_app['client_id'],
                                                                           client_secret=spotify_app['client_secret']))
        return sp

    def get_id(self, track) -> str:
        if track['spotify_id'] is None:
            track['spotify_id'] = self.get_api().search(q=f'{track["artist"]} {track["title"]}')['tracks']['items'][0]['id']
        return track['spotify_id']

    def get_track_by_yt_track(self, yt_track) -> dict:
        return self.get_api().search(q=f'{yt_track["artists"][0]["name"]} {yt_track["title"]}')['tracks']['items'][0]

    def get_audio_features(self, spotify_id) -> dict:
        return self.get_api().audio_features(spotify_id)[0]

    def get_popularity(self, track) -> int:
        return self.get_api().track(self.get_id(track))['popularity']

    def get_popularities(self, tracks) -> list:
        # split into chunks of 50
        chunks = [tracks[i:i + 50] for i in range(0, len(tracks), 50)]
        popularities = []

        for chunk in chunks:
            ids = [track['spotify_id'] for track in chunk]
            results = self.get_api().tracks(ids)['tracks']
            popularities.extend([result['popularity'] for result in results])

        if len(popularities) != len(tracks):
            raise Exception("Length of popularities does not match length of tracks")
        return popularities