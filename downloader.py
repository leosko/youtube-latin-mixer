import os
import time
from multiprocessing.pool import ThreadPool

import yt_dlp
from pathvalidate import sanitize_filepath, sanitize_filename

from database import Database


def progress_hook(d):
    if d['status'] == 'finished':
        print('Done downloading from YouTube, now converting to music file...')
    if d['status'] == 'error':
        print('Error downloading from YouTube')


class Downloader:
    def __init__(self, database: Database, library_path: str = "tmp", thread_count: int = 1, force_download: bool = False):
        self.database = database
        self.library_path = library_path
        if not os.path.exists(library_path):
            os.makedirs(library_path)
        self.thread_count = thread_count
        self.force_download = force_download
        self.database = database

    def download_yt(self, track):
        audio_file_path = self.get_path(track)
        if not self.should_download(track):
            return 0
        res = yt_dlp.YoutubeDL({
            'format': 'm4a/bestaudio/best',
            'postprocessors': [{
                'key': 'FFmpegExtractAudio',
                'preferredcodec': 'm4a'
            }],
            'progress_hooks': [progress_hook],
            'outtmpl': audio_file_path,
            'ignoreerrors': True,
        }, ).download([f"https://www.youtube.com/watch?v={track['video_id']}"])
        if res != 0:
            return {"error": res, "track": track}
        return 0

        # with warnings.catch_warnings():
        #     warnings.simplefilter("ignore")  # ignore librosa warnings, because it fallbacks to "slower" method, but we don't care
        #     y, sr = librosa.load(audio_file_path)
        #     tempo, beat_frames = librosa.beat.beat_track(y=y, sr=sr)
        #     track['bpm'] = tempo

    def download_tracks(self, tracks):
        tracks_to_download = list(filter(lambda x: self.should_download(x), tracks))
        with ThreadPool(processes=self.thread_count) as pool:
            for result in pool.map(self.download_yt, tracks_to_download):
                if result is not None and result != 0:
                    print(f"Error downloading {result['track']['video_id']}: {result['error']}")
                    if result['track']['unavailable_since'] is None:
                        result['track']['unavailable_since'] = time.time()
                        self.database.update_track(result['track'])

    def should_download(self, track):
        return self.force_download or (
                not os.path.exists(self.get_path(track)) and track['unavailable_since'] is None
        )

    def get_path(self, track):
        filename = sanitize_filename(f"{track['genre'].short_str()} - {track['video_id']} - "
                                     f"{track['artist']} - {track['title']}.m4a")
        return sanitize_filepath(f"{self.library_path}{os.sep}{filename}")
