# How to start

1. `git clone`
2. `cd` into the directory
3. `pip install -r requirements.txt`
4. `ytmusicapi oauth`, see https://ytmusicapi.readthedocs.io/en/latest/setup/oauth.html
5. Create app at https://developer.spotify.com/dashboard, copy `client_id` and `client_secret` and paste into `spotify_app.json` as a JSON object
6. `python main.py`