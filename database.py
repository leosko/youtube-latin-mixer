import datetime
import json
import sqlite3
from math import log
from typing import Union, Dict

from Levenshtein import distance

from shuffle_plan import Source, Genre


def get_track_from_row_from_db(row, index=None):
    res = {
        'video_id': row[0],
        'json_from_youtube': json.loads(row[1]),
        'genre': Genre[row[2].upper()],
        'title': row[3],
        'artist': row[4],
        'priority': row[5],
        'bpm': row[6],
        'mood': row[7],
        'spotify_id': row[8],
        'data_from_spotify': json.loads(row[9]) if row[9] else None,
        'danceability': row[10],
        'energy': row[11],
        'key': row[12],
        'loudness': row[13],
        'mode': row[14],
        'instrumentalness': row[15],
        'acousticness': row[16],
        'liveness': row[17],
        'valence': row[18],
        'duration_seconds': row[19],
        'speechiness': row[20],
        'verified': False if row[21] == '0' else True,
        'unavailable_since': row[22],
        'source': Source[row[23].upper()],
        'published_at': row[24],
        'view_count': row[25],
        'popularity': row[26],
        'index': index,
    }
    res['_duration'] = datetime.timedelta(seconds=res['duration_seconds'])
    res['_popularity'] = log(res['popularity']) if (res['popularity'] is not None and res['popularity'] > 2) else 0
    if res['published_at'] is not None and res['published_at'] != '0':
        res['_published_at_datetime'] = datetime.datetime.strptime(res['published_at'], '%Y-%m-%d')
        res['_age'] = (datetime.datetime.now() - res['_published_at_datetime'])
    else:
        res['_published_at_datetime'] = datetime.datetime.now() - datetime.timedelta(days=365 * 5)
        res['_age'] = datetime.timedelta(days=365 * 5)
    res['_spotify_url'] = f"https://open.spotify.com/track/{res['spotify_id']}" if res['spotify_id'] else None
    res['_youtube_url'] = f"https://www.youtube.com/watch?v={res['video_id']}"
    return res


class Database:

    def __init__(self, db_url):
        self.con = sqlite3.connect(db_url)
        self.con.row_factory = sqlite3.Row
        self.cur = self.con.cursor()

        self.average_values = None
        self.tracks = None

    def insert_track(self, track):
        print(f"Adding {track} to db, url: https://www.youtube.com/watch?v={track['video_id']}, "
              f"spotify: https://open.spotify.com/track/{track['spotify_id']}")
        self.cur.execute("insert into tracks values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?,"
                         "?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                         (track["video_id"], json.dumps(track["json_from_youtube"]), track["genre"].name.lower(),
                          track["title"], track["artist"], track["priority"], track["bpm"], track["mood"],

                          track["spotify_id"], json.dumps(track["data_from_spotify"]),
                          track["danceability"], track["energy"], track["key"], track["loudness"], track["mode"],
                          track["instrumentalness"], track["acousticness"], track["liveness"], track["valence"],
                          track["duration_seconds"], track["speechiness"], track["verified"],
                          track["unavailable_since"], track["source"].name.lower(), track["published_at"],
                          track["view_count"], track["popularity"]))
        self.con.commit()

    def get_average_values(self):
        if self.average_values is None:
            row = self.cur.execute(
                "select avg(bpm), avg(danceability), avg(energy), avg(key), "
                "avg(loudness), avg(mode), avg(instrumentalness), "
                "avg(acousticness), avg(liveness), avg(valence), "
                "avg(duration_seconds), avg(speechiness) from tracks").fetchone()
            self.average_values = dict(zip(map(lambda x: x.split('(')[1].split(')')[0], row.keys()), row))
        return self.average_values

    def load_tracks(self):
        res = self.cur.execute("select * from tracks")
        list_res = []
        # iterate over the rows with index
        for i, row in enumerate(res):
            list_res.append(get_track_from_row_from_db(row, i))
        return list_res

    def update_track(self, track_from_db):
        print(f"Updating {track_from_db['video_id']} in db")
        res = self.cur.execute("""update tracks set 
                            json_from_youtube = ?, genre = ?, 
                            title = ?, artist = ?,
                            priority = ?, bpm = ?, mood = ?, 

                            spotify_id = ?, data_from_spotify = ?,
                            danceability = ?, energy = ?, key = ?, loudness = ?, mode = ?,
                            instrumentalness = ?, acousticness = ?, liveness = ?, valence = ?,
                            duration_seconds = ?, speechiness = ?, 
                            unavailable_since = ?, source = ?, published_at = ?, view_count = ?,
                            popularity = ?

                            where video_id = ?""",
                               (json.dumps(track_from_db["json_from_youtube"]), track_from_db['genre'].name.lower(),
                                track_from_db['title'], track_from_db['artist'],
                                track_from_db['priority'], track_from_db['bpm'], track_from_db['mood'],

                                track_from_db['spotify_id'], json.dumps(track_from_db['data_from_spotify']),
                                track_from_db['danceability'], track_from_db['energy'], track_from_db['key'],
                                track_from_db['loudness'], track_from_db['mode'],
                                track_from_db['instrumentalness'], track_from_db['acousticness'],
                                track_from_db['liveness'],
                                track_from_db['valence'],
                                track_from_db['duration_seconds'], track_from_db['speechiness'],
                                track_from_db['unavailable_since'], track_from_db['source'].name.lower(),
                                track_from_db['published_at'], track_from_db['view_count'],
                                track_from_db['popularity'],

                                track_from_db['video_id']))
        self.con.commit()
        return res

    def delete_track(self, youtube_id: str):
        print(f"Removing {youtube_id} in db")
        res = self.cur.execute("""delete from tracks
                            where video_id = ?""",
                               (youtube_id,))
        self.con.commit()
        return res

    def get_tracks(self):
        if self.tracks is None:
            self.tracks = self.load_tracks()
            print(f"Loaded {len(self.tracks)} tracks from DB")
        return self.tracks

    def reload_tracks(self):
        self.tracks = None
        self.get_tracks()

    def get_track_by_yt_video_id(self, video_id) -> Union[Dict, None]:
        return next(filter(lambda x: x['video_id'] == video_id, self.get_tracks()), None)

    def get_track_by_spotify_id(self, spotify_id) -> Union[Dict, None]:
        return next(filter(lambda x: x['spotify_id'] == spotify_id, self.get_tracks()), None)

    def get_closest_track_by_artist_title(self, artist, title, album=None) -> Union[Dict, None]:
        min_distance = float('inf')
        closest_track = None
        for track in self.get_tracks():
            artist_distance = distance(track['artist'], artist)
            title_distance = distance(track['title'], title)
            album_name = track['json_from_youtube']['album']['name'] if ('album' in track['json_from_youtube'] and track['json_from_youtube']['album'] is not None) else None
            album_distance = distance(album_name, album) if album is not None and album_name is not None else 0
            total_distance = artist_distance + title_distance + album_distance
            if total_distance < min_distance:
                min_distance = total_distance
                closest_track = track
        if min_distance > (len(artist) + len(title) + (len(album) if album else 0)) // 4:
            return None  # no close matches
        return closest_track

    def print_stats(self):
        res = self.cur.execute(
            "SELECT genre, COUNT(*), AVG(bpm), AVG(danceability), AVG(popularity), sum(duration_seconds) "
            "FROM tracks "
            "GROUP BY genre"
        )
        print(list(map(lambda x: f"{x[0]}x{x[1]}, bpm={x[2]}, danceability={x[3]}, "
                                 f"popularity={x[4]}, duration_seconds={x[5]}", res.fetchall())))
